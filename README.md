# Standard module

## Packages

<!-- AUTO-GENERATED-CONTENT:START (SUBPACKAGELIST:verbose=true) -->
* [@koober/database-client](packages/database-client) - Database client library
* [@koober/http-client](packages/http-client) - HTTP Client module
* [@koober/log](packages/log) - Logging library
* [@koober/std](packages/std) - Standard library
* [@koober/uuid](packages/uuid) - UUID generator
<!-- AUTO-GENERATED-CONTENT:END -->
