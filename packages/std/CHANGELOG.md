# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# 1.0.0-rc.54 (2021-09-01)


### Features

* **std:** add throwError function ([5cbc2d4](https://gitlab.com/koober-sas/std/commit/5cbc2d45445f61b9a12f47c0a4f934855a6978e7))





# 1.0.0-rc.53 (2021-08-12)

**Note:** Version bump only for package @koober/std





# 1.0.0-rc.51 (2021-08-11)

**Note:** Version bump only for package @koober/std





# [1.0.0-rc.50](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.49...v1.0.0-rc.50) (2021-08-09)


### Bug Fixes

* correct some runTask typing errors ([70d87ff](https://gitlab.com/koober-sas/std/commit/70d87ff1fafcc0594431633472316a2154d60631))





# 1.0.0-rc.49 (2021-08-09)


### Features

* add Task.andRun ([1d9e330](https://gitlab.com/koober-sas/std/commit/1d9e330e447ce641ce340f585b468ea0de233440))





# [1.0.0-rc.48](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.47...v1.0.0-rc.48) (2021-08-04)

**Note:** Version bump only for package @koober/std





# [1.0.0-rc.47](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.46...v1.0.0-rc.47) (2021-08-04)


### Features

* add DataObject.MakeGeneric ([92a21cd](https://gitlab.com/koober-sas/std/commit/92a21cd499c636859b6266178e588413022dc33d))
* **std:** add DataError module ([0927b7e](https://gitlab.com/koober-sas/std/commit/0927b7e8a93b9225bc94a914de82d2fd6738a79a))





# 1.0.0-rc.46 (2021-08-04)


### Bug Fixes

* **std:** make Task properties readonly ([742fc53](https://gitlab.com/koober-sas/std/commit/742fc5371d6ed3dceb6ccfc8d2353aefa570ea68))





# 1.0.0-rc.45 (2021-07-22)


### Features

* **std:** make task constructors depends on Task.Result interface ([eec9c68](https://gitlab.com/koober-sas/std/commit/eec9c68b65adf4f5047451193b34ce169859789c))





# 1.0.0-rc.44 (2021-07-21)


### Features

* **std:** allow awaitable value and error for Task.Async.tryCall ([04fb285](https://gitlab.com/koober-sas/std/commit/04fb28537140842b30fd3107c8945b3418bc792c))





# [1.0.0-rc.43](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.42...v1.0.0-rc.43) (2021-07-20)


### Features

* **std:** make Time.delay cancelable ([34ada4f](https://gitlab.com/koober-sas/std/commit/34ada4fb09bf217333bbb1fbd3027d12941e3ef4))





# 1.0.0-rc.42 (2021-07-20)


### Bug Fixes

* use type for Ref instead of interface ([6ffef63](https://gitlab.com/koober-sas/std/commit/6ffef638ad4436087613ef0a81b2c97442b5705b))





# 1.0.0-rc.41 (2021-07-20)

**Note:** Version bump only for package @koober/std





# 1.0.0-rc.40 (2021-07-19)

**Note:** Version bump only for package @koober/std





# 1.0.0-rc.39 (2021-07-13)

**Note:** Version bump only for package @koober/std





# [1.0.0-rc.38](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.37...v1.0.0-rc.38) (2021-07-12)

**Note:** Version bump only for package @koober/std





# [1.0.0-rc.37](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.36...v1.0.0-rc.37) (2021-07-12)

**Note:** Version bump only for package @koober/std





# [1.0.0-rc.36](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.35...v1.0.0-rc.36) (2021-07-12)

**Note:** Version bump only for package @koober/std





# [1.0.0-rc.35](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.34...v1.0.0-rc.35) (2021-07-12)

**Note:** Version bump only for package @koober/std





# [1.0.0-rc.34](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.33...v1.0.0-rc.34) (2021-07-12)

**Note:** Version bump only for package @koober/std





# 1.0.0-rc.33 (2021-07-12)

**Note:** Version bump only for package @koober/std





# [1.0.0-rc.32](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.31...v1.0.0-rc.32) (2021-07-05)


### Features

* improve error handling ([69da2d9](https://gitlab.com/koober-sas/std/commit/69da2d94d33971b469bd99319d28b511551ef54a))





# [1.0.0-rc.31](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.30...v1.0.0-rc.31) (2021-07-05)


### Features

* use result object for constructors ([51261eb](https://gitlab.com/koober-sas/std/commit/51261eb7784fd70114e1a7b8e9a48ea9914c2f0f))





# [1.0.0-rc.30](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.29...v1.0.0-rc.30) (2021-07-05)

**Note:** Version bump only for package @koober/std





# [1.0.0-rc.28](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.27...v1.0.0-rc.28) (2021-07-01)


### Features

* add abs() ([ad19957](https://gitlab.com/koober-sas/std/commit/ad1995799c4d42add4c619a51e84877a187662d9))
* add Array.flatMap ([40819e0](https://gitlab.com/koober-sas/std/commit/40819e0d18db0fea98660bdc94a550627efb9e2c))
* add Array.flatMap ([562f221](https://gitlab.com/koober-sas/std/commit/562f2213242a90da85f00c613b9dcd1e3c97889e))
* add Array.hasInstance ([9ed3996](https://gitlab.com/koober-sas/std/commit/9ed3996a4585b382e11aa150ad46c8e70ae99b36))
* add canceler in API ([fa6950b](https://gitlab.com/koober-sas/std/commit/fa6950b7164ad2343023d58a3c692fd05468fb6a))
* add Math.ceil ([919b818](https://gitlab.com/koober-sas/std/commit/919b818da611e85737a74726ea5dee433b924279))
* add Math.floor function ([baef649](https://gitlab.com/koober-sas/std/commit/baef6494550e913cbd62ab341fe21f8ebe968eaf))
* add Math.round ([353d57c](https://gitlab.com/koober-sas/std/commit/353d57c65f38a02c6c3232b02a96805408f54834))
* add Math.truncate method ([cc47d05](https://gitlab.com/koober-sas/std/commit/cc47d057c13b7568a1e2ee541fef7dbca529e104))
* add Random module ([a739abb](https://gitlab.com/koober-sas/std/commit/a739abbf197cff949b8536d547c2e1719424ae5d))
* add Ref module ([c28c9ce](https://gitlab.com/koober-sas/std/commit/c28c9ceac0648a27edd0b0d8f45f4d19eb634fa9))
* add Time.delay ([f94b377](https://gitlab.com/koober-sas/std/commit/f94b377e80caec4cbfd078bdc4d03707ba4664e7))
* add Time.hasInstance and Duration.hasInstance ([af0e071](https://gitlab.com/koober-sas/std/commit/af0e071001096204ef28a4c71e0bd4f398ebd216))
* add Time.parseISOString ([40405b4](https://gitlab.com/koober-sas/std/commit/40405b4781818131aefd78a743043b657c58b883))
* throw invariant error when wrong value given to Duration() and Time() ([776355e](https://gitlab.com/koober-sas/std/commit/776355ef9dcb0889be49cdca897aadb139d49ca2))





# [1.0.0-rc.27](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.26...v1.0.0-rc.27) (2021-05-17)


### Bug Fixes

* **std:** remove global overloading ([f065c9e](https://gitlab.com/koober-sas/std/commit/f065c9e6137135f62138c8f999e6fda00071c35b))





# [1.0.0-rc.26](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.25...v1.0.0-rc.26) (2021-05-17)


### Bug Fixes

* **std:** correct filter typing ([cbc8926](https://gitlab.com/koober-sas/std/commit/cbc8926954f90b778b95382d3cb2680dbe56d457))





# [1.0.0-rc.25](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.24...v1.0.0-rc.25) (2021-05-10)


### Bug Fixes

* **std:** correct some constructor inference ([a186ce9](https://gitlab.com/koober-sas/std/commit/a186ce904081eb27ab2f3424e804b170a85c9d9c))


### Features

* add Iterable.zip ([d307266](https://gitlab.com/koober-sas/std/commit/d307266d0f7d693d96126b7a21ebc73bf6ae06b2))
* **std:** add Iterable.empty() ([bf7cb98](https://gitlab.com/koober-sas/std/commit/bf7cb988596114f154665d6cfcfcc98157bedec0))
* add Console module ([5df3059](https://gitlab.com/koober-sas/std/commit/5df3059094bf88e2f61d1dfd72535a6c3254831c))
* add Int.parse and Int.stringify ([257aeb1](https://gitlab.com/koober-sas/std/commit/257aeb1b3d5d126648c810243f2ef8a00520965c))
* add int8 and uint8 ([c6521da](https://gitlab.com/koober-sas/std/commit/c6521daf0072a0f053e8e0a38a98d70efbb3df33))
* add Iterable.range ([e0aa6a2](https://gitlab.com/koober-sas/std/commit/e0aa6a20117a975c7885b9a16c0c45d9d098835e))
* add objectId function ([5e40eb0](https://gitlab.com/koober-sas/std/commit/5e40eb0e973fdc313fe56601d00c1bb865f6b2fb))
* add Option.from function ([06ebeb0](https://gitlab.com/koober-sas/std/commit/06ebeb017324d38410a2c80242c8ec41b62819a7))
* **std:** add assign and extend functions ([ba47b20](https://gitlab.com/koober-sas/std/commit/ba47b20ed6bad276fe3c3eb90b693c52e57343cc))
* **std:** add integer module ([865652b](https://gitlab.com/koober-sas/std/commit/865652b0cf77c6e3a0b81369ee7a2de05897800d))
* add Result.tryCall ([7d7afe4](https://gitlab.com/koober-sas/std/commit/7d7afe45a6841fb632f2c6baff03c1844f14256d))
* add time module ([9745759](https://gitlab.com/koober-sas/std/commit/974575910fbfc86453d18ebacee8ace18ecea95e))
* **std:** add Array.concat ([300c2dd](https://gitlab.com/koober-sas/std/commit/300c2dd3202d947515b64d623a1d93fd485d183d))
* **std:** add Array.reverse ([8872d45](https://gitlab.com/koober-sas/std/commit/8872d45dd0aec7e30a0397a5a4a2751e2b1e82c2))
* **std:** add Task.tryCall ([e1e9cbb](https://gitlab.com/koober-sas/std/commit/e1e9cbbe06aaf0d673fe74cc0fb6b7a966cedc60))
* **std:** make array public ([19b84cf](https://gitlab.com/koober-sas/std/commit/19b84cfa046f92ec97edf7013c0e301b3f0d329d))
* add readonly array module ([e0be316](https://gitlab.com/koober-sas/std/commit/e0be316894422d6bed459b5035963620fa0a69e9))





# [1.0.0-rc.24](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.23...v1.0.0-rc.24) (2021-03-26)


### Features

* **std:** add hash module ([7cb6743](https://gitlab.com/koober-sas/std/commit/7cb67437c3190e38eae69231cb4e90655890139d))
* **std:** add pipe in prelude module ([d8f678f](https://gitlab.com/koober-sas/std/commit/d8f678fadce75dd5e57290812664584a554a8f62))
* **std:** add Task.{Async, Sync}.of  constructors ([76a5b50](https://gitlab.com/koober-sas/std/commit/76a5b50b434b3f85e049b03acdda15cc4a37f915))





# [1.0.0-rc.23](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.22...v1.0.0-rc.23) (2021-03-25)


### Features

* **std:** add Option.andThen and Option.orElse ([7ca2e08](https://gitlab.com/koober-sas/std/commit/7ca2e08ad7bafb0d807f1ca5009621df69722065))
* **std:** add Result.andThen and Result.orElse ([0aa29b4](https://gitlab.com/koober-sas/std/commit/0aa29b4f08f8d2a9d1d25d00902c258be3a63812))
* **std:** add Task.orElse ([f937135](https://gitlab.com/koober-sas/std/commit/f937135f6077993a8e159d4d7f073f0075a4b2f0))
* **std:** make task typing easier ([066aa37](https://gitlab.com/koober-sas/std/commit/066aa37b2b4900f51b76ffdd2bc49af68987dc33))





# [1.0.0-rc.22](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.21...v1.0.0-rc.22) (2021-03-08)


### Bug Fixes

* **std:** correct tests ([f9dd3be](https://gitlab.com/koober-sas/std/commit/f9dd3be1cb76c8d3e58df97582ad358ffc3e297b))
* **std:** correct tests ([92045b2](https://gitlab.com/koober-sas/std/commit/92045b2967cdda8a9b7bfeca0cf9f07c23470fde))


### Features

* **std:** add assertType helper ([8cec9fe](https://gitlab.com/koober-sas/std/commit/8cec9fe6f9b5edc93312981072d0703cf3fd69b8))
* **std:** add Async constructor ([c816ab5](https://gitlab.com/koober-sas/std/commit/c816ab5e510f441c6b5180a074e379c82d016d2b))
* **std:** add async to Task.map ([9e49a1d](https://gitlab.com/koober-sas/std/commit/9e49a1dba37a49245ff59330dfef1d54dbf286b3))
* **std:** add runTask ([97705b6](https://gitlab.com/koober-sas/std/commit/97705b652a617ec05eb8d8bc2f9ead87c4524aae))
* **std:** add task constructor ([66c676f](https://gitlab.com/koober-sas/std/commit/66c676f052f83f137456121ea73b8013d5035bce))
* **std:** add Task to public api ([ff2824a](https://gitlab.com/koober-sas/std/commit/ff2824ab790b3a287516f12b99b61bfb63878f76))
* **std:** add Task.map ([6b10d70](https://gitlab.com/koober-sas/std/commit/6b10d701fd4f98aad9d552e50ddd1876c36d3293))
* **std:** add Task.mapError ([56a63c7](https://gitlab.com/koober-sas/std/commit/56a63c727ab02ae024b361eebc63bfbeba9c9a2f))
* **std:** add Task.Sync constructor ([e7fb68c](https://gitlab.com/koober-sas/std/commit/e7fb68c983ba6ba761cda5464a8dd4dfcb9ba7d8))





# [1.0.0-rc.21](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.20...v1.0.0-rc.21) (2021-02-15)


### Bug Fixes

* **lint:** resolve no-shadow errors ([a94c251](https://gitlab.com/koober-sas/std/commit/a94c25160da23226ac721cb1dec6ac71819c7cf0))
* **std:** correct Option.map typing ([5d98ed3](https://gitlab.com/koober-sas/std/commit/5d98ed344d056b58c555783bc7999ccbbdeff7fd))


### Features

* **std:** add dict module ([0039102](https://gitlab.com/koober-sas/std/commit/003910241c69cfb4491a07c7be39f093f4af3988))
* **std:** add index export wrapper ([649a3f8](https://gitlab.com/koober-sas/std/commit/649a3f87800b50fd76d465eeadca567ef292690c))


### Reverts

* Revert "chore(deps): upgrade eslint config" ([05258f6](https://gitlab.com/koober-sas/std/commit/05258f60f786cf8275dd132878fb265a512aee86))





# [1.0.0-rc.20](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.19...v1.0.0-rc.20) (2020-09-24)


### Bug Fixes

* **build:** correct missing check task ([566b4bd](https://gitlab.com/koober-sas/std/commit/566b4bd3cf9c2a3bb74e31207bb83a3361828080))





# [1.0.0-rc.19](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.18...v1.0.0-rc.19) (2020-09-21)

**Note:** Version bump only for package @koober/std





# [1.0.0-rc.18](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.17...v1.0.0-rc.18) (2020-09-21)


### Bug Fixes

* **lint:** ignore some doc errors ([c210a63](https://gitlab.com/koober-sas/std/commit/c210a6344c237fafae2bfdf0cea38e7840b65812))





# [1.0.0-rc.17](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.16...v1.0.0-rc.17) (2020-09-15)


### Bug Fixes

* **eslint:** correct new errors ([4a115e7](https://gitlab.com/koober-sas/std/commit/4a115e7d0b58bb6383aa69e81a170ee3e37c8b56))


### Features

* **assert:** add invariant helper ([6d5026e](https://gitlab.com/koober-sas/std/commit/6d5026e7c176221ebcbd2c37ccb37b4c669f39cb))
* **assert:** import assertNever method ([df4fb1c](https://gitlab.com/koober-sas/std/commit/df4fb1c5d2fb181dd224331cefc343ef3c6ba7c1))





# [1.0.0-rc.16](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.15...v1.0.0-rc.16) (2020-08-28)

**Note:** Version bump only for package @koober/std





# 1.0.0-rc.15 (2020-08-28)


### Features

* **std:** add option module ([c55787e](https://gitlab.com/koober-sas/std/commit/c55787e73ab1e73ff73dc3977a3899e0f9fad8bf))
* **std:** add Option.map and Option.unwrap ([af3aa72](https://gitlab.com/koober-sas/std/commit/af3aa72fce889c973f5f34fea9ea5f67b9e37e82))
* **std:** add result (rust like) monad ([3b434bf](https://gitlab.com/koober-sas/std/commit/3b434bf2c0550a836a26afcaaa6c45fa74fc6724))
* **std:** add Result.unwrap function ([9e6e686](https://gitlab.com/koober-sas/std/commit/9e6e686aec9c48784f1084bfd2f5fb9f89a97629))
* **std:** add simple Algebraic Data Type implementation ([17490df](https://gitlab.com/koober-sas/std/commit/17490df505f5be1292893d7d74994a0ebe9eabf1))
