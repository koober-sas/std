import { Ref } from './ref';

describe(Ref, () => {
  test('should return the current value', () => {
    expect(Ref(123)).toEqual({ [Ref.value]: 123 });
  });
  describe(Ref.read, () => {
    test('should return current value', () => {
      const ref = Ref(123);
      expect(Ref.read(ref)).toEqual(123);
    });
  });
  describe(Ref.write, () => {
    test('should set current value', () => {
      const ref = Ref(123);
      Ref.write(ref, 456);
      expect(ref).toEqual({ [Ref.value]: 456 });
    });
  });
});
