export interface Ref<Value> {
  /**
   * Mutable reference to a value
   */
  [Ref.value]: Value;
}
/**
 * Create a new `Ref` object containing a value
 *
 * @example
 * ```typescript
 * const ref = Ref(123);// { [Ref.value]: initialValue }
 * ```
 * @category Constructor
 * @param initialValue the initial value contained
 */
export function Ref<Value>(initialValue: Value): Ref<Value> {
  return { [Ref.value]: initialValue };
}
export namespace Ref {
  /**
   * Current value symbol
   */
  export const value = Symbol('refValue');

  /**
   * Returns the current ref value
   *
   * @example
   *```typescript
   * const ref = Ref('foo');
   * Ref.read(ref); // 'foo'
   * ```
   *
   * @category Accessor
   * @param ref the reference object
   */
  export function read<Value>(ref: Ref<Value>): Value {
    return ref[value];
  }

  /**
   * Change the current value
   *
   * @example
   *```typescript
   * const ref = Ref('foo');
   * Ref.write(ref, 'bar'); // Ref.read(ref) == 'bar'
   * ```
   *
   * @category Accessor
   * @param ref the reference object
   * @param newValue the new value to be set
   */
  export function write<Value>(ref: Ref<Value>, newValue: Value): void {
    ref[value] = newValue;
  }
}
