import { log, debug, critical } from '@koober/log';

// Example of data to log
const foo = { bar: 'baz' };

log(critical`This is really bad ${{ foo }}`); // > This is really bad { bar: 'baz' }
log(debug`Some Debug ${{ hey: 'debug info' }}`); // > This is really bad "debug info"
