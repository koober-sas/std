import { addHandler, consoleHandler, fileHandler, LogHandler, LogLevel, logger, debug, error } from '@koober/log';

if (require.main === module) {
  // 1. Configure any handler
  // Example #1 a handler that will print to console any log with level superior or equal to warning
  addHandler(LogHandler.filter((record) => LogLevel.compare(record, LogLevel.Warning) >= 0)(consoleHandler));

  // Example #2 a handler that will write to file `MyComponent.log` every events from logger named `MyComponent`
  addHandler(
    LogHandler.filter((record) => record.category === 'MyComponent')(
      fileHandler({ filename: require.resolve('./MyComponent.log') })
    )
  );

  // 2. Run the program
  const log = logger('MyComponent');
  log(debug`Some Debug`);
  log(error`Some Error`);
}
