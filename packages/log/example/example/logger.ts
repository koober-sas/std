import { critical, logger } from '@koober/log';

/**
 * Logger
 */
const log = logger('MyComponent');

export class MyComponent {
  run(): void {
    const toto = {};

    log(critical`This is really bad ${{ toto }}`);
  }
}
