import { LogRecord, logger, debug } from '@koober/log';

// Example of middleware that adds a customData to each log record
const customDataMiddleware = (logRecord: LogRecord): LogRecord => ({
  ...logRecord,
  data: {
    ...logRecord.data,
    customData: 'custom',
  },
});

/**
 * Logger
 */
const log = logger('MyComponent', [customDataMiddleware]);
log(debug`This is a debug message`); // > This is a debug message
// `data.customData` will be available in any handler
