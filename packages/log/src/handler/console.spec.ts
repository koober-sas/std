/* eslint-disable no-console */

import { LogLevel } from '../level';
import { LogMessage } from '../message';
import { generateLogRecord } from '../__stub__';
import { consoleHandler } from './console';

describe('.consoleHandler', () => {
  const noop = (): void => undefined;
  jest.spyOn(console, 'debug').mockImplementation(noop);
  jest.spyOn(console, 'trace').mockImplementation(noop);
  jest.spyOn(console, 'info').mockImplementation(noop);
  jest.spyOn(console, 'warn').mockImplementation(noop);
  jest.spyOn(console, 'error').mockImplementation(noop);

  test('should send to console.debug when level=LogLevel.Debug', () => {
    consoleHandler(generateLogRecord({ level: LogLevel.Debug, message: ['test'] }));
    expect(console.debug).toHaveBeenLastCalledWith('test');
  });

  test('should send to console.info when level=LogLevel.Info', () => {
    consoleHandler(generateLogRecord({ level: LogLevel.Info, message: ['test'] }));
    expect(console.info).toHaveBeenLastCalledWith('test');
  });

  test('should send to console.warn when level=LogLevel.Warning', () => {
    consoleHandler(generateLogRecord({ level: LogLevel.Warning, message: ['test'] }));
    expect(console.warn).toHaveBeenLastCalledWith('test');
  });

  test('should send to console.warn when level=LogLevel.Error', () => {
    consoleHandler(generateLogRecord({ level: LogLevel.Error, message: ['test'] }));
    expect(console.error).toHaveBeenLastCalledWith('test');
  });

  test('should format logCategory and logMessage correctly', () => {
    consoleHandler(
      generateLogRecord({
        category: 'logCategory',
        level: LogLevel.Debug,
        message: LogMessage(['message', LogMessage.Ref('foo', 'bar')]),
      })
    );
    expect(console.debug).toHaveBeenLastCalledWith('[logCategory]', 'message', 'bar');
  });

  test('should not add logCategory if empty', () => {
    consoleHandler(
      generateLogRecord({
        level: LogLevel.Debug,
        message: LogMessage(['message', LogMessage.Ref('foo', 'bar')]),
      })
    );
    expect(console.debug).toHaveBeenLastCalledWith('message', 'bar');
  });
});
