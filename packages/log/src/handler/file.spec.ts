import { promises as fs } from 'fs';
import * as os from 'os';
import * as path from 'path';
import { LogRecord } from '../record';
import { generateLogRecord } from '../__stub__';
import { fileHandler } from './file';

describe('.fileHandler', () => {
  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  const createFsMock = () => ({
    open: jest.fn().mockResolvedValue({ close: jest.fn().mockResolvedValue(undefined) }),
    writeFile: jest.fn().mockResolvedValue(undefined),
    mkdir: jest.fn().mockResolvedValue(undefined),
  });

  const withFile = (block: (temp: string) => unknown): (() => Promise<void>) => {
    const generateFilename = (): string => `${os.tmpdir}/fileHandler/test-${Math.random()}`;

    return async () => {
      const pathString = generateFilename();
      try {
        await block(pathString);
      } finally {
        await fs.unlink(pathString).catch(() => undefined);
      }
    };
  };
  test('should create directory', async () => {
    const anyRecord = generateLogRecord();
    const fsMock = createFsMock();
    await fileHandler({
      fs: fsMock,
      filename: 'foo/bar/file',
    })(anyRecord);

    expect(fsMock.mkdir).toHaveBeenLastCalledWith('foo/bar', { recursive: true });
  });

  test('should create directory when filename is a function', async () => {
    const anyRecord = generateLogRecord();
    const fsMock = createFsMock();
    await fileHandler({
      fs: fsMock,
      filename: () => 'foo/bar/file',
    })(anyRecord);

    expect(fsMock.mkdir).toHaveBeenLastCalledWith('foo/bar', { recursive: true });
  });

  test('should writeFile using JSON.stringify', async () => {
    const anyRecord = generateLogRecord();
    const fsMock = createFsMock();
    await fileHandler({
      fs: fsMock,
      filename: 'file',
    })(anyRecord);

    expect(fsMock.writeFile).toHaveBeenLastCalledWith(expect.any(Object), `${JSON.stringify(anyRecord)}\n`, undefined);
  });

  test('should writeFile using format function if defined', async () => {
    const anyRecord = generateLogRecord();
    const customFormat = ({ created }: LogRecord): string => `${JSON.stringify({ created })}\n`;
    const fsMock = createFsMock();
    await fileHandler({
      fs: fsMock,
      filename: 'filename',
      format: customFormat,
    })(anyRecord);

    expect(fsMock.writeFile).toHaveBeenLastCalledWith(
      expect.any(Object),
      `${JSON.stringify({ created: anyRecord.created })}\n`,
      undefined
    );
  });

  test(
    'should write into file if directory does not exist',
    withFile(async (temporaryFile) => {
      const anyRecord = generateLogRecord();
      await fileHandler({
        filename: temporaryFile,
      })(anyRecord);

      expect(await fs.readFile(temporaryFile, { encoding: 'utf8' })).toEqual(`${JSON.stringify(anyRecord)}\n`);
    })
  );

  test(
    'should write into file if directory already existing',
    withFile(async (temporaryFile) => {
      const anyRecord = generateLogRecord();

      await fs.mkdir(path.dirname(temporaryFile), { recursive: true });
      await fileHandler({
        filename: temporaryFile,
      })(anyRecord);

      expect(await fs.readFile(temporaryFile, { encoding: 'utf8' })).toEqual(`${JSON.stringify(anyRecord)}\n`);
    })
  );
});
