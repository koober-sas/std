import { Time, Task } from '@koober/std';
import { UUID } from '@koober/uuid';
import { Logger, LoggerParams, loggerParamsFactory } from './logger';
import { LogDispatcher } from './dispatcher';
import { LogLevel } from './level';
import { LogMessage } from './message';

describe('Logger', () => {
  const anyDispatcher = LogDispatcher<any>();
  const currentTime = Task.Sync(jest.fn(({ ok }) => ok(Time(0))));
  const randomUUID = Task.Sync(jest.fn(({ ok }) => ok(UUID.empty())));
  jest.spyOn(anyDispatcher, 'handle');

  const anyLogger = Logger.create({
    loggerName: 'loggerName',
    loggerMiddlewares: [],
    dispatcher: anyDispatcher,
    currentTime,
    randomUUID,
  });

  describe('.create()', () => {
    test('should return a new logger', () => {
      expect(
        Logger.create({
          loggerName: 'loggerName',
          loggerMiddlewares: [],
          dispatcher: anyDispatcher,
          currentTime,
          randomUUID,
        })
      ).toEqual({
        loggerName: 'loggerName',
        dispatcher: anyDispatcher,
        loggerMiddlewares: [],
        currentTime,
        randomUUID,
      });
    });
  });

  describe('.handleRecord()', () => {
    test('should call dispatcher handle', () => {
      Logger.handleRecord(anyLogger, {
        data: { foo: true },
        levelName: LogLevel.stringify(LogLevel.Warning),
        level: LogLevel.value(LogLevel.Warning),
        message: LogMessage(['test']),
      });

      expect(anyLogger.dispatcher.handle).toHaveBeenCalledWith({
        id: UUID.empty(),
        category: anyLogger.loggerName,
        data: { foo: true },
        levelName: LogLevel.stringify(LogLevel.Warning),
        level: LogLevel.value(LogLevel.Warning),
        message: LogMessage(['test']),
        created: 0,
      });
    });
    test('should apply middlewares', () => {
      const logger = Logger.create({
        ...anyLogger,
        loggerMiddlewares: [
          (event) => ({
            ...event,
            data: {
              ...event.data,
              bar: true,
            },
          }),
        ],
      });

      Logger.handleRecord(logger, {
        data: { foo: true },
        levelName: LogLevel.stringify(LogLevel.Warning),
        level: LogLevel.value(LogLevel.Warning),
        message: LogMessage(['test']),
      });

      expect(logger.dispatcher.handle).toHaveBeenCalledWith({
        id: UUID.empty(),
        category: logger.loggerName,
        data: { foo: true, bar: true },
        levelName: LogLevel.stringify(LogLevel.Warning),
        level: LogLevel.value(LogLevel.Warning),
        message: LogMessage(['test']),
        created: 0,
      });
    });
  });
});

describe('LoggerParams', () => {
  describe('.create()', () => {
    test('should return a new logger params instance', () => {
      expect(
        LoggerParams.create({
          level: 1,
          levelName: 'Test',
          message: ['foo'],
          data: {
            var: true,
          },
        })
      ).toEqual({
        level: 1,
        levelName: 'Test',
        message: ['foo'],
        data: {
          var: true,
        },
        withData: expect.any(Function),
      });
    });

    test('should add withData() method', () => {
      expect(
        LoggerParams.create({
          level: 1,
          levelName: 'Test',
          message: ['foo'],
          data: {
            var: true,
          },
        }).withData({
          var2: true,
        })
      ).toEqual({
        level: 1,
        levelName: 'Test',
        message: ['foo'],
        data: {
          var: true,
          var2: true,
        },
        withData: expect.any(Function),
      });
    });
  });
});

describe('loggerParamsFactory()', () => {
  const anyLevel = { level: 10, levelName: 'AnyLevel' };

  test('should return a factory of LoggerParams', () => {
    const factory = loggerParamsFactory(anyLevel);
    expect(factory`This is a message`).toEqual({
      level: anyLevel.level,
      levelName: anyLevel.levelName,
      message: ['This is a message'],
      data: {},
      withData: expect.any(Function),
    });
  });
  test('should parse references from string template', () => {
    const factory = loggerParamsFactory(anyLevel);
    expect(factory`This is a message ${{ foo: 'bar' }} !`).toEqual({
      level: anyLevel.level,
      levelName: anyLevel.levelName,
      message: ['This is a message ', LogMessage.Ref('foo', 'bar'), ' !'],
      data: {
        foo: 'bar',
      },
      withData: expect.any(Function),
    });
  });
  test('should handle empty references', () => {
    const factory = loggerParamsFactory(anyLevel);
    // eslint-disable-next-line unicorn/no-null
    [{}, undefined, null].forEach((emptyReference) => {
      expect(factory`This is a message ${emptyReference}!`).toEqual({
        level: anyLevel.level,
        levelName: anyLevel.levelName,
        message: ['This is a message !'],
        data: {},
        withData: expect.any(Function),
      });
    });
  });
  test('should handle multiple keys in a reference', () => {
    const factory = loggerParamsFactory(anyLevel);
    expect(factory`This is a message ${{ foo: 'bar', baz: true }} !`).toEqual({
      level: anyLevel.level,
      levelName: anyLevel.levelName,
      message: ['This is a message ', LogMessage.Ref('foo', 'bar'), LogMessage.Ref('baz', true), ' !'],
      data: {
        foo: 'bar',
        baz: true,
      },
      withData: expect.any(Function),
    });
  });

  test('should handle string concatenation', () => {
    const factory = loggerParamsFactory(anyLevel);
    const anyString = 'anyString';
    expect(factory`Test ${anyString} ${{ foo: 'bar' }} !`).toEqual({
      level: anyLevel.level,
      levelName: anyLevel.levelName,
      message: ['Test anyString ', LogMessage.Ref('foo', 'bar'), ' !'],
      data: {
        foo: 'bar',
      },
      withData: expect.any(Function),
    });
  });
});
