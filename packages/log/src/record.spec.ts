import { Time } from '@koober/std';
import { UUID } from '@koober/uuid';
import { LogRecord } from './record';
import { LogLevel } from './level';
import { LogMessage } from './message';

describe(LogRecord, () => {
  describe('()', () => {
    test('should return a new message', () => {
      expect(
        LogRecord({
          id: UUID.empty(),
          category: 'category',
          levelName: LogLevel.stringify(LogLevel.Warning),
          level: LogLevel.value(LogLevel.Warning),
          message: LogMessage(['foo', 'bar', '']),
          data: {},
          created: Time(1),
        })
      ).toEqual({
        id: UUID.empty(),
        category: 'category',
        levelName: LogLevel.stringify(LogLevel.Warning),
        level: LogLevel.value(LogLevel.Warning),
        message: ['foobar'],
        data: {},
        created: 1,
      });
    });
  });
});
