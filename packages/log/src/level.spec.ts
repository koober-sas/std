import { LogLevel } from './level';

describe(LogLevel, () => {
  describe(LogLevel.value, () => {
    test(`should return value for ${LogLevel.Critical.levelName}`, () => {
      expect(LogLevel.value(LogLevel.Critical)).toBe(50);
    });
    test(`should return value for ${LogLevel.Error.levelName}`, () => {
      expect(LogLevel.value(LogLevel.Error)).toBe(40);
    });
    test(`should return value for ${LogLevel.Warning.levelName}`, () => {
      expect(LogLevel.value(LogLevel.Warning)).toBe(30);
    });
    test(`should return value for ${LogLevel.Info.levelName}`, () => {
      expect(LogLevel.value(LogLevel.Info)).toBe(20);
    });

    test(`should return value for ${LogLevel.Debug.levelName}`, () => {
      expect(LogLevel.value(LogLevel.Debug)).toBe(10);
    });
  });

  describe(LogLevel.compare, () => {
    test('should return 1 if superior', () => {
      expect(LogLevel.compare(LogLevel.Error, LogLevel.Warning)).toBe(1);
    });

    test('should return 0 if equal', () => {
      expect(LogLevel.compare(LogLevel.Error, LogLevel.Error)).toBe(0);
    });

    test('should return -1 if inferior', () => {
      expect(LogLevel.compare(LogLevel.Warning, LogLevel.Error)).toBe(-1);
    });
  });

  describe(LogLevel.match, () => {
    test('should return undefined if empty cases', () => {
      expect(LogLevel.match([])(LogLevel.Critical.level)).toBe(undefined);
    });
    test('should return default value if defined', () => {
      expect(LogLevel.match([], 'defaultValue')(LogLevel.Critical.level)).toBe('defaultValue');
    });
    test('should return value if level exact value is found', () => {
      const matcherFunction = LogLevel.match([
        [LogLevel.Warning, 'foo'],
        [LogLevel.Error, 'bar'],
        [LogLevel.Critical, 'baz'],
      ]);
      expect(matcherFunction(LogLevel.Error.level)).toBe('bar');
    });
    test('should select first superior or equal matcher', () => {
      const matcherFunction = LogLevel.match([
        [LogLevel.Warning, 'foo'],
        [LogLevel.Error, 'bar'],
        [LogLevel.Critical, 'baz'],
      ]);

      expect(matcherFunction(LogLevel.Error.level)).toBe('bar');
      expect(matcherFunction(LogLevel.Warning.level)).toBe('foo');
      expect(matcherFunction(LogLevel.Info.level)).toBe(undefined);
    });

    test('should return defaultValue if no matcher is fulfilled', () => {
      const matcherFunction = LogLevel.match(
        [
          [LogLevel.Warning, 'foo'],
          [LogLevel.Error, 'bar'],
          [LogLevel.Critical, 'baz'],
        ],
        'defaultValue'
      );
      expect(matcherFunction(LogLevel.Info.level)).toBe('defaultValue');
    });
  });
});
