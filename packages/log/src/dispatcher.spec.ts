import { LogDispatcher } from './dispatcher';
import { generateLogRecord } from './__stub__';

describe('LogDispatcher', () => {
  const createDispatcher = LogDispatcher;
  const anyRecord = generateLogRecord({});

  describe('handle()', () => {
    test('should call all handlers', () => {
      const handler = jest.fn();
      const dispatcher = createDispatcher({ initialState: { handlers: [handler] } });
      dispatcher.handle(anyRecord);

      expect(handler).toHaveBeenCalledWith(anyRecord);
    });
  });

  describe('setHandlers()', () => {
    test('should replace handlers', () => {
      const dispatcher = createDispatcher({ initialState: { handlers: [jest.fn()] } });
      const handlers = [jest.fn(), jest.fn()];
      dispatcher.setHandlers(handlers);
      expect(dispatcher.getHandlers()).toEqual(handlers);
    });
  });

  describe('addHandler()', () => {
    test('should add at the end a new handler', () => {
      const dispatcher = createDispatcher();
      const newHandler = jest.fn();
      dispatcher.addHandler(newHandler);
      expect(dispatcher.getHandlers()).toEqual([newHandler]);
    });

    test('should append handler to handler list', () => {
      const defaultHandler = jest.fn();
      const dispatcher = createDispatcher({
        initialState: {
          handlers: [defaultHandler],
        },
      });

      const newHandler = jest.fn();
      dispatcher.addHandler(newHandler);
      expect(dispatcher.getHandlers()).toEqual([defaultHandler, newHandler]);
    });
  });

  describe('removeHandler()', () => {
    test('should remove handler from state', () => {
      const defaultHandler = jest.fn();
      const dispatcher = createDispatcher({
        initialState: {
          handlers: [defaultHandler],
        },
      });

      dispatcher.removeHandler(defaultHandler);
      expect(dispatcher.getHandlers()).toEqual([]);
    });
  });
});
