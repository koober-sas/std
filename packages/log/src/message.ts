import { DataObject } from '@koober/std';

export interface LogMessage extends ReadonlyArray<LogMessage.Item> {}

/**
 * Construct LogMessage
 *
 * @param parts - constructor parameters
 * @returns a new LogMessage
 */
export function LogMessage(parts: ReadonlyArray<LogMessage.Item>): LogMessage {
  /* eslint-disable unicorn/no-for-loop */

  const returnValue = [];
  let buffer = '';
  for (let index = 0; index < parts.length; index += 1) {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const value = parts[index]!;
    if (typeof value === 'string') {
      buffer += value;
    } else {
      if (buffer.length > 0) {
        returnValue.push(buffer);
        buffer = '';
      }
      returnValue.push(value);
    }
  }
  if (buffer.length > 0) {
    returnValue.push(buffer);
  }

  return returnValue;
}
export namespace LogMessage {
  export interface Ref<V = unknown>
    extends DataObject<{
      /**
       * Type tag
       */
      [DataObject.type]: 'LogMessageRef';
      /**
       * Reference name
       */
      name: string;

      /**
       * Reference value
       */
      value: V;
    }> {}
  /**
   * Construct Ref
   *
   * @param name - the reference name
   * @param value - the reference value
   * @returns a new structure
   */
  export function Ref<V>(name: string, value: V): Ref<V> {
    return { _type: Ref.typeName, name, value };
  }
  Ref.typeName = 'LogMessageRef' as const;

  export type Item = string | Ref;

  /**
   * Return an object of all refs
   *
   * @param message - the message
   * @returns object dict
   */
  export function data(message: LogMessage): { [key: string]: unknown } {
    const returnValue: { [key: string]: unknown } = {};
    // eslint-disable-next-line unicorn/no-for-loop
    for (let index = 0; index < message.length; index += 1) {
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      const part = message[index]!;
      if (typeof part !== 'string') {
        returnValue[part.name] = part.value;
      }
    }

    return returnValue;
  }
}
