import type { Time } from '@koober/std';
import type { UUID } from '@koober/uuid';
import { LogMessage } from './message';
import { LogLevel } from './level';

export interface LogRecord extends LogLevel {
  /**
   * Event identifier
   */
  readonly id: UUID;
  /**
   * The log category / logger name
   */
  readonly category: string;
  /**
   * Additional data
   */
  readonly data: Readonly<{ [key: string]: unknown }>;
  /**
   * Unformatted message
   */
  readonly message: LogMessage;
  /**
   * Time when the record was created
   */
  readonly created: Time;
}

/**
 * Construct LogRecord
 *
 * @param data - constructor parameters
 * @returns a new LogRecord
 */
export function LogRecord({
  id,
  category,
  data,
  levelName,
  level,
  message,
  created,
}: {
  id: LogRecord['id'];
  category: LogRecord['category'];
  data: LogRecord['data'];
  levelName: LogRecord['levelName'];
  level: LogRecord['level'];
  message: LogRecord['message'];
  created: LogRecord['created'];
}): LogRecord {
  return { id, category, data, levelName, level, message, created };
}
