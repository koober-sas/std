/* eslint-disable import/export */
/* eslint-disable no-redeclare */

export function LogDispatcher<T>({
  initialState = LogDispatcher.initialState,
}: {
  initialState?: LogDispatcher.State<T>;
} = {}): LogDispatcher.Module<T> {
  let state: LogDispatcher.State<T> = initialState;

  function getState(): LogDispatcher.State<T> {
    return state;
  }

  function setState(updater: (currentState: LogDispatcher.State<T>) => LogDispatcher.State<T>): void {
    state = updater(state);
  }

  async function callHandler(handlerFunction: LogDispatcher.Handler<T>, event: T): Promise<void> {
    return handlerFunction(event);
  }

  function getHandlers(): Array<LogDispatcher.Handler<T>> {
    return [...state.handlers];
  }

  function setHandlers(handlers: Array<LogDispatcher.Handler<T>>): void {
    setState((currentState) => ({ ...currentState, handlers: [...handlers] }));
  }

  function addHandler(handler: LogDispatcher.Handler<T>): void {
    setState((currentState) => ({ ...currentState, handlers: [...currentState.handlers, handler] }));
  }

  function removeHandler(handler: LogDispatcher.Handler<T>): void {
    setState((currentState) => ({ ...currentState, handlers: currentState.handlers.filter((_) => _ !== handler) }));
  }

  function handle(record: T): Promise<void> {
    const { handlers } = getState();
    const promises = handlers.map((handler) => callHandler(handler, record));

    return Promise.all(promises).then((_) => undefined);
  }

  return {
    handle,
    addHandler,
    removeHandler,
    setHandlers,
    getHandlers,
  };
}
export namespace LogDispatcher {
  export type Handler<T> = (record: T) => Promise<void> | void;

  export interface Module<T> {
    /**
     * Send a record to all handlers
     *
     * @param record - the log record to handle
     */
    handle(record: T): void;

    /**
     * Add a new handler to the module handlers
     *
     * @param handler - the new handler
     */
    addHandler(handler: Handler<T>): void;

    /**
     * Remove a handler from the module handlers
     *
     * @param handler - the handler to remove
     */
    removeHandler(handler: Handler<T>): void;

    /**
     * Return a list of all handlers
     */
    getHandlers(): Array<Handler<T>>;

    /**
     * Replace all module handlers by `handlers`
     *
     * @param handlers - the new handlers
     */
    setHandlers(handlers: Array<Handler<T>>): void;
  }

  export type State<T> = {
    handlers: ReadonlyArray<Handler<T>>;
  };

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  export const initialState: State<any> = {
    handlers: [],
  };
}
