import { LogLevel } from './level';
import { warning, debug, info, error, critical } from '.';

describe('index', () => {
  test('should have a debug factory', () => {
    expect(debug`test`).toEqual(
      expect.objectContaining({
        level: LogLevel.Debug.level,
        levelName: LogLevel.Debug.levelName,
        message: ['test'],
      })
    );
  });
  test('should have a info factory', () => {
    expect(info`test`).toEqual(
      expect.objectContaining({
        level: LogLevel.Info.level,
        levelName: LogLevel.Info.levelName,
        message: ['test'],
      })
    );
  });
  test('should have a warning factory', () => {
    expect(warning`test`).toEqual(
      expect.objectContaining({
        level: LogLevel.Warning.level,
        levelName: LogLevel.Warning.levelName,
        message: ['test'],
      })
    );
  });
  test('should have a error factory', () => {
    expect(error`test`).toEqual(
      expect.objectContaining({
        level: LogLevel.Error.level,
        levelName: LogLevel.Error.levelName,
        message: ['test'],
      })
    );
  });
  test('should have a critical factory', () => {
    expect(critical`test`).toEqual(
      expect.objectContaining({
        level: LogLevel.Critical.level,
        levelName: LogLevel.Critical.levelName,
        message: ['test'],
      })
    );
  });
});
