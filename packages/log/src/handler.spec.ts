import { UUID } from '@koober/uuid';
import { LogHandler } from './handler';
import { LogRecord } from './record';
import { generateTime } from './__stub__';

describe('LogHandler', () => {
  describe(LogHandler.filter, () => {
    test('should filter input', async () => {
      const handler = jest.fn();
      const filtered = LogHandler.filter((record) => record.category === 'foo')(handler);
      const defaultProps = LogRecord({
        id: UUID.empty(),
        category: 'not_foo',
        level: 1,
        levelName: 'WARNING',
        created: generateTime(),
        message: [],
        data: {},
      });
      await filtered(
        LogRecord({
          ...defaultProps,
          category: 'not_foo',
        })
      );
      expect(handler).not.toHaveBeenCalled();
      await filtered(
        LogRecord({
          ...defaultProps,
          category: 'foo',
        })
      );
      expect(handler).not.toHaveBeenLastCalledWith({
        id: '',
        category: 'foo',
      });
    });
  });
});
