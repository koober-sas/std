import { Time } from '@koober/std';
import { UUID } from '@koober/uuid';
import { LogRecord } from './record';
import { LogLevel } from './level';
import { LogMessage } from './message';

export const generateTime = (ms = 0) => Time(ms);

export const generateLogRecord = ({
  level = LogLevel.Debug,
  message = LogMessage(['test']),
  category = '',
}: Partial<
  Pick<LogRecord, 'message' | 'category'> & {
    level?: LogLevel;
  }
> = {}): LogRecord =>
  LogRecord({
    id: UUID.empty(),
    category,
    levelName: LogLevel.stringify(level),
    level: LogLevel.value(level),
    message,
    data: {},
    created: generateTime(),
  });
