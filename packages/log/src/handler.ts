import { LogRecord } from './record';

export type LogHandler = {
  (logRecord: LogRecord): Promise<void> | void;
};
export namespace LogHandler {
  /**
   * Decorate handler where `predicate` is applied on record input.
   * If `false`, the handler is never called
   *
   * @param predicate - the predicate applied on log record
   */
  export function filter(predicate: (record: LogRecord) => boolean): (handler: LogHandler) => LogHandler {
    return (handler) => (logRecord) => predicate(logRecord) ? handler(logRecord) : undefined;
  }
}
