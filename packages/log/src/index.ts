import { LogLevel } from './level';
import { LogDispatcher } from './dispatcher';
import { loggerFactory, loggerParamsFactory } from './logger';
import { LogRecord } from './record';

// Init dispatcher module
const logDispatcher = LogDispatcher<LogRecord>();

export const setHandlers = logDispatcher.setHandlers.bind(logDispatcher);

export const addHandler = logDispatcher.addHandler.bind(logDispatcher);

export const removeHandler = logDispatcher.removeHandler.bind(logDispatcher);

/**
 * Logger factory function
 *
 * @example
 *
 * const log = logger('MyComponent', []);
 *
 * @param loggerName - the name of the logger added to each record
 * @param middlewares - an array of all middlewares
 */
export const logger = loggerFactory(logDispatcher);

/**
 * Default logger with empty name and no middleware
 *
 * @example
 *
 * log(error`This is an error message !`);
 *
 * @param loggerParameters - partial log record data
 */
export const log = logger('', []);

/**
 * `Critical` string template
 *
 * @example
 * critical`My message ${{ foo: 'bar' }}`
 * critical`My message ${{ foo: 'bar' }}`.withData({ baz: true })
 */
export const critical = loggerParamsFactory(LogLevel.Critical);

/**
 * `Error` string template
 *
 * @example
 * error`My message ${{ foo: 'bar' }}`
 * error`My message ${{ foo: 'bar' }}`.withData({ baz: true })
 */
export const error = loggerParamsFactory(LogLevel.Error);

/**
 * `Warning` string template
 *
 * @example
 * warning`My message ${{ foo: 'bar' }}`
 * warning`My message ${{ foo: 'bar' }}`.withData({ baz: true })
 */
export const warning = loggerParamsFactory(LogLevel.Warning);

/**
 * `Info` string template
 *
 * @example
 * info`My message ${{ foo: 'bar' }}`
 * info`My message ${{ foo: 'bar' }}`.withData({ baz: true })
 */
export const info = loggerParamsFactory(LogLevel.Info);

/**
 * `Debug` string template
 *
 * @example
 * debug`My message ${{ foo: 'bar' }}`
 * debug`My message ${{ foo: 'bar' }}`.withData({ baz: true })
 */
export const debug = loggerParamsFactory(LogLevel.Debug);

// Forward exports
export * from './level';
export * from './message';
export * from './record';
export * from './handler';
export * from './handler/console';
export * from './handler/file';
