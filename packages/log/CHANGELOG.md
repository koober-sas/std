# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# 1.0.0-rc.54 (2021-09-01)

**Note:** Version bump only for package @koober/log





# 1.0.0-rc.53 (2021-08-12)


### Bug Fixes

* setup correctly tsconfig.json ([7054880](https://gitlab.com/koober-sas/std/commit/70548804e7078d3f4ffd4d4fbcdc4b38fbf496b7))





# [1.0.0-rc.52](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.51...v1.0.0-rc.52) (2021-08-11)

**Note:** Version bump only for package @koober/log





# 1.0.0-rc.51 (2021-08-11)

**Note:** Version bump only for package @koober/log





# [1.0.0-rc.50](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.49...v1.0.0-rc.50) (2021-08-09)

**Note:** Version bump only for package @koober/log





# 1.0.0-rc.49 (2021-08-09)


### Features

* add Task.andRun ([1d9e330](https://gitlab.com/koober-sas/std/commit/1d9e330e447ce641ce340f585b468ea0de233440))





# [1.0.0-rc.48](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.47...v1.0.0-rc.48) (2021-08-04)

**Note:** Version bump only for package @koober/log





# [1.0.0-rc.47](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.46...v1.0.0-rc.47) (2021-08-04)

**Note:** Version bump only for package @koober/log





# 1.0.0-rc.46 (2021-08-04)

**Note:** Version bump only for package @koober/log





# 1.0.0-rc.45 (2021-07-22)


### Features

* **std:** make task constructors depends on Task.Result interface ([eec9c68](https://gitlab.com/koober-sas/std/commit/eec9c68b65adf4f5047451193b34ce169859789c))





# 1.0.0-rc.44 (2021-07-21)

**Note:** Version bump only for package @koober/log





# [1.0.0-rc.43](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.42...v1.0.0-rc.43) (2021-07-20)

**Note:** Version bump only for package @koober/log





# 1.0.0-rc.42 (2021-07-20)

**Note:** Version bump only for package @koober/log





# 1.0.0-rc.41 (2021-07-20)

**Note:** Version bump only for package @koober/log





# 1.0.0-rc.40 (2021-07-19)

**Note:** Version bump only for package @koober/log





# 1.0.0-rc.39 (2021-07-13)

**Note:** Version bump only for package @koober/log





# [1.0.0-rc.38](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.37...v1.0.0-rc.38) (2021-07-12)

**Note:** Version bump only for package @koober/log





# [1.0.0-rc.37](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.36...v1.0.0-rc.37) (2021-07-12)

**Note:** Version bump only for package @koober/log





# [1.0.0-rc.36](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.35...v1.0.0-rc.36) (2021-07-12)

**Note:** Version bump only for package @koober/log





# [1.0.0-rc.35](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.34...v1.0.0-rc.35) (2021-07-12)

**Note:** Version bump only for package @koober/log





# [1.0.0-rc.34](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.33...v1.0.0-rc.34) (2021-07-12)

**Note:** Version bump only for package @koober/log





# 1.0.0-rc.33 (2021-07-12)

**Note:** Version bump only for package @koober/log





# [1.0.0-rc.32](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.31...v1.0.0-rc.32) (2021-07-05)

**Note:** Version bump only for package @koober/log





# [1.0.0-rc.31](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.30...v1.0.0-rc.31) (2021-07-05)

**Note:** Version bump only for package @koober/log





# [1.0.0-rc.30](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.29...v1.0.0-rc.30) (2021-07-05)

**Note:** Version bump only for package @koober/log





# [1.0.0-rc.28](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.27...v1.0.0-rc.28) (2021-07-01)

**Note:** Version bump only for package @koober/log





# [1.0.0-rc.27](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.26...v1.0.0-rc.27) (2021-05-17)

**Note:** Version bump only for package @koober/log





# [1.0.0-rc.26](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.25...v1.0.0-rc.26) (2021-05-17)

**Note:** Version bump only for package @koober/log





# [1.0.0-rc.25](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.24...v1.0.0-rc.25) (2021-05-10)

**Note:** Version bump only for package @koober/log





# [1.0.0-rc.24](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.23...v1.0.0-rc.24) (2021-03-26)

**Note:** Version bump only for package @koober/log





# [1.0.0-rc.21](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.20...v1.0.0-rc.21) (2021-02-15)


### Reverts

* Revert "chore(deps): upgrade eslint config" ([05258f6](https://gitlab.com/koober-sas/std/commit/05258f60f786cf8275dd132878fb265a512aee86))





# [1.0.0-rc.20](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.19...v1.0.0-rc.20) (2020-09-24)


### Bug Fixes

* **build:** correct missing check task ([566b4bd](https://gitlab.com/koober-sas/std/commit/566b4bd3cf9c2a3bb74e31207bb83a3361828080))


### Features

* **log:** add lazy filename evaluation for file handler ([f6dfa32](https://gitlab.com/koober-sas/std/commit/f6dfa3233ac75ae96cdcaacee38d648b7ba4de5f))





# [1.0.0-rc.18](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.17...v1.0.0-rc.18) (2020-09-21)


### Bug Fixes

* **lint:** ignore some doc errors ([c210a63](https://gitlab.com/koober-sas/std/commit/c210a6344c237fafae2bfdf0cea38e7840b65812))
* **ts:** correct typescript errors ([c0bd49d](https://gitlab.com/koober-sas/std/commit/c0bd49de608bc05847e153c6db77cacbf35e6b5f))





# [1.0.0-rc.17](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.16...v1.0.0-rc.17) (2020-09-15)


### Bug Fixes

* **eslint:** correct new errors ([4a115e7](https://gitlab.com/koober-sas/std/commit/4a115e7d0b58bb6383aa69e81a170ee3e37c8b56))





# 1.0.0-rc.15 (2020-08-28)


### Bug Fixes

* **deps:** update dependency uuid to v8 ([3e61433](https://gitlab.com/koober-sas/std/commit/3e614334b628f14136acfe4811c4c0ce65326551))
* **lint:** correct naming errors ([5b4c622](https://gitlab.com/koober-sas/std/commit/5b4c622704f06c03851082d6226b5ef618bffd34))
