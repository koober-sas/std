import * as Fetch from '.';

describe('index', () => {
  test('exports', () => {
    expect(Object.keys(Fetch).sort()).toEqual(
      ['HTTPClient', 'parseArrayBuffer', 'parseBlob', 'parseFormData', 'parseJSON', 'parseText'].sort()
    );
  });
});
