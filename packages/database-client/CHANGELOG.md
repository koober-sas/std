# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# 1.0.0-rc.54 (2021-09-01)

**Note:** Version bump only for package @koober/database-client





# 1.0.0-rc.53 (2021-08-12)


### Bug Fixes

* setup correctly tsconfig.json ([7054880](https://gitlab.com/koober-sas/std/commit/70548804e7078d3f4ffd4d4fbcdc4b38fbf496b7))





# 1.0.0-rc.51 (2021-08-11)

**Note:** Version bump only for package @koober/database-client





# [1.0.0-rc.50](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.49...v1.0.0-rc.50) (2021-08-09)

**Note:** Version bump only for package @koober/database-client





# 1.0.0-rc.49 (2021-08-09)


### Features

* add Task.andRun ([1d9e330](https://gitlab.com/koober-sas/std/commit/1d9e330e447ce641ce340f585b468ea0de233440))





# [1.0.0-rc.48](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.47...v1.0.0-rc.48) (2021-08-04)

**Note:** Version bump only for package @koober/database-client





# [1.0.0-rc.47](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.46...v1.0.0-rc.47) (2021-08-04)

**Note:** Version bump only for package @koober/database-client





# 1.0.0-rc.46 (2021-08-04)

**Note:** Version bump only for package @koober/database-client





# 1.0.0-rc.45 (2021-07-22)


### Features

* **std:** make task constructors depends on Task.Result interface ([eec9c68](https://gitlab.com/koober-sas/std/commit/eec9c68b65adf4f5047451193b34ce169859789c))





# 1.0.0-rc.44 (2021-07-21)

**Note:** Version bump only for package @koober/database-client





# [1.0.0-rc.43](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.42...v1.0.0-rc.43) (2021-07-20)

**Note:** Version bump only for package @koober/database-client





# 1.0.0-rc.42 (2021-07-20)

**Note:** Version bump only for package @koober/database-client





# 1.0.0-rc.41 (2021-07-20)

**Note:** Version bump only for package @koober/database-client





# 1.0.0-rc.40 (2021-07-19)

**Note:** Version bump only for package @koober/database-client





# 1.0.0-rc.39 (2021-07-13)

**Note:** Version bump only for package @koober/database-client





# [1.0.0-rc.38](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.37...v1.0.0-rc.38) (2021-07-12)

**Note:** Version bump only for package @koober/database-client





# [1.0.0-rc.37](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.36...v1.0.0-rc.37) (2021-07-12)

**Note:** Version bump only for package @koober/database-client





# [1.0.0-rc.36](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.35...v1.0.0-rc.36) (2021-07-12)

**Note:** Version bump only for package @koober/database-client





# [1.0.0-rc.35](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.34...v1.0.0-rc.35) (2021-07-12)

**Note:** Version bump only for package @koober/database-client





# [1.0.0-rc.34](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.33...v1.0.0-rc.34) (2021-07-12)

**Note:** Version bump only for package @koober/database-client





# 1.0.0-rc.33 (2021-07-12)

**Note:** Version bump only for package @koober/database-client





# [1.0.0-rc.32](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.31...v1.0.0-rc.32) (2021-07-05)

**Note:** Version bump only for package @koober/database-client





# [1.0.0-rc.31](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.30...v1.0.0-rc.31) (2021-07-05)

**Note:** Version bump only for package @koober/database-client





# [1.0.0-rc.30](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.29...v1.0.0-rc.30) (2021-07-05)

**Note:** Version bump only for package @koober/database-client





# [1.0.0-rc.29](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.28...v1.0.0-rc.29) (2021-07-02)


### Features

* add DatabaseClientError as public error ([cda68e6](https://gitlab.com/koober-sas/std/commit/cda68e6a247aeef98739b1f5afa97b54a39d735c))





# [1.0.0-rc.28](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.27...v1.0.0-rc.28) (2021-07-01)

**Note:** Version bump only for package @koober/database-client





# [1.0.0-alpha.3](https://gitlab.com/koober-sas/database/compare/v1.0.0-alpha.1...v1.0.0-alpha.3) (2021-02-23)

**Note:** Version bump only for package @koober/database-client





# 1.0.0-alpha.1 (2020-08-31)

**Note:** Version bump only for package @koober/database-client
