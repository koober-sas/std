import { SQL, executeQuery, DatabaseClient } from '@koober/database-client';
import { runTask, Task } from '@koober/std';

interface User {
  id: number;
  name: string;
}

export function getUserById(client: DatabaseClient, id: User['id']) {
  const sqlStatement = SQL`SELECT id, name FROM user WHERE id=${id}`;
  const task = executeQuery(client, sqlStatement);

  return Task.map(task, (rows) => {
    return Array.isArray(rows) ? (rows[0] as User) : undefined;
  });
}

export async function main(): Promise<void> {
  const client: DatabaseClient = {
    databaseType: 'mysql',
    database: '',
    user: '',
  };

  const response = getUserById(client, 123);
  console.log(await runTask(response));
}
