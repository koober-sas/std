import { Result, runTask } from '@koober/std';
import { executeQuery } from './execute';
import { DatabaseClientError } from './error';
import { SQL } from './sql';
import { SQLQuery } from './query';

describe(executeQuery, () => {
  const anyQuery = SQL`SELECT id from table`;
  const createClient = () =>
    ({
      databaseType: 'mock',
      mockExecuteQuery: jest.fn(() => Promise.reject<unknown>(new Error('NotImplemented'))),
      mockQueryToStatement: jest.fn(SQLQuery.toSQLStatement),
    } as const);

  test('should forward query execution to environment', async () => {
    const client = createClient();
    await runTask(executeQuery(client, anyQuery));
    expect(client.mockExecuteQuery).toHaveBeenCalledWith(anyQuery);
  });

  test('should return Result.Ok of environment.executeQuery if promise resolved', async () => {
    const client = createClient();
    client.mockExecuteQuery.mockResolvedValue('TestReturn');
    await expect(runTask(executeQuery(client, anyQuery))).resolves.toEqual(Result.Ok('TestReturn'));
  });

  test('should return Result.Error of environment.executeQuery if promise rejected', async () => {
    const client = createClient();
    client.mockExecuteQuery.mockReturnValue(Promise.reject('MockError')); // eslint-disable-line prefer-promise-reject-errors
    await expect(runTask(executeQuery(client, anyQuery))).resolves.toEqual(
      Result.Error(DatabaseClientError({ cause: 'MockError' }))
    );
  });

  test('should convert to sql statement', async () => {
    const client = createClient();
    client.mockExecuteQuery.mockResolvedValue(() => []);
    await runTask(executeQuery(client, SQLQuery.CreateSchema({ schemaName: 'test' })));
    expect(client.mockExecuteQuery).toHaveBeenLastCalledWith(SQL`CREATE SCHEMA test`);
  });
});
