import { DataError } from '@koober/std';

export interface DatabaseClientError
  extends DataError<{
    name: 'DatabaseClientError';
  }> {}
export const DatabaseClientError = DataError.Make<DatabaseClientError>('DatabaseClientError');
