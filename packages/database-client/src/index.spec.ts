import * as Module from '.';

describe('index', () => {
  test('exports', () => {
    expect(Object.keys(Module).sort()).toEqual(
      [
        // List of all public exports
        'DatabaseClientError',
        'SQL',
        'SQLDataType',
        'SQLQuery',
        'SQLStatement',
        'executeQuery',
        'isSQLStatement',
      ].sort()
    );
  });
});
