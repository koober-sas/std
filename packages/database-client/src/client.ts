import { SQLStatement } from './sql';
import { SQLQuery } from './query';
import { DatabaseClientError } from './error';

/**
 * The environment represents the way to connect to a database server
 */
export interface AbstractDatabaseClient<Name extends string> {
  databaseType: Name;
}

/**
 * The environment represents the way to connect to a database server
 */
export interface AbstractDatabaseAdapter<Name extends string> {
  adapter: Name;
  queryToStatement(query: SQLQuery): SQLStatement;
  executeQuery(sqlStatement: SQLStatement): Promise<unknown>;
  handleError(innerError: unknown): Promise<DatabaseClientError>;
}
