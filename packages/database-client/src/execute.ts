import { Task } from '@koober/std';
import { DatabaseClient, getDatabaseAdapter } from './adapter';
import { SQLStatement, isSQLStatement } from './sql';
import { SQLQuery } from './query';
import { DatabaseClientError } from './error';

/**
 * Execute the `sqlStatement` on an `client`
 *
 * @example
 * ```typescript
 * const query = executeQuery(client, SQLQuery.CreateTable({ tableName: 'test_table' }))
 * const result = await runTask(query);
 * if (Result.isOk(result)) {
 *   console.log(result.value)
 * } else {
 *   console.error(result.error);
 * }
 * ```
 *
 * @param client created with a database adapter `createEnvironment(environmentConfig)` function
 * @param sqlOrQuery SQL query object or a raw sql statement
 */
export function executeQuery(
  client: DatabaseClient,
  sqlOrQuery: SQLStatement | SQLQuery
): Task.Async<unknown, DatabaseClientError> {
  const adapter = getDatabaseAdapter(client);

  return Task.Async.tryCall(async () => {
    const sqlStatement = isSQLStatement(sqlOrQuery) ? sqlOrQuery : adapter.queryToStatement(sqlOrQuery);

    return adapter.executeQuery(sqlStatement);
  }, adapter.handleError.bind(adapter));
}
