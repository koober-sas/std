import { getDatabaseAdapter } from '.';

jest.mock('sqlite3', () => ({
  Database: (() => {
    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    function MockDatabase() {
      return undefined;
    }

    return MockDatabase;
  })(),
}));

describe(getDatabaseAdapter, () => {
  test('should return environment when adapter is "mysql"', () => {
    expect(getDatabaseAdapter({ databaseType: 'mysql' })).toEqual({
      adapter: 'mysql',
      executeQuery: expect.any(Function),
      queryToStatement: expect.any(Function),
      handleError: expect.any(Function),
    });
  });
  test('should return environment when adapter is "sqlite3"', () => {
    expect(getDatabaseAdapter({ databaseType: 'sqlite3', filename: 'filename' })).toEqual({
      adapter: 'sqlite3',
      executeQuery: expect.any(Function),
      queryToStatement: expect.any(Function),
      handleError: expect.any(Function),
    });
  });
  test('should return environment when adapter is "mock"', () => {
    const mockExecuteQuery = () => Promise.resolve();
    expect(
      getDatabaseAdapter({
        databaseType: 'mock',
        mockExecuteQuery,
      })
    ).toEqual({
      adapter: 'mock',
      executeQuery: mockExecuteQuery,
      queryToStatement: expect.any(Function),
      handleError: expect.any(Function),
    });
  });
});
