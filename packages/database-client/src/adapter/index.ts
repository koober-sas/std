/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-require-imports */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable global-require, @typescript-eslint/no-var-requires */
import { assertNever } from '@koober/std';
import { AbstractDatabaseAdapter } from '../client';
import { MySQLClient } from './mysql';
import { SQLite3Client } from './sqlite3';
import { MockClient } from './mock';

export type DatabaseClient = MySQLClient | SQLite3Client | MockClient;

/**
 * Create an adapter from configuration
 *
 * @param client
 */
export function getDatabaseAdapter(client: DatabaseClient): AbstractDatabaseAdapter<DatabaseClient['databaseType']> {
  switch (client.databaseType) {
    case 'mysql':
      return require('./mysql').MySQLAdapter(client);
    case 'sqlite3':
      return require('./sqlite3').SQLite3Adapter(client);
    case 'mock':
      return require('./mock').MockAdapter(client);
    default:
      return assertNever(client);
  }
}
