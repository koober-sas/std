import { createConnection, ConnectionConfig } from 'mysql';
import { SQLStatement } from '../sql';
import type { AbstractDatabaseAdapter, AbstractDatabaseClient } from '../client';
import { SQLQuery } from '../query';
import { DatabaseClientError } from '../error';

export interface MySQLClient extends AbstractDatabaseClient<'mysql'>, ConnectionConfig {}

export interface MySQLAdapter extends AbstractDatabaseAdapter<'mysql'> {}

export function MySQLAdapter(client: MySQLClient): MySQLAdapter {
  const adapter = 'mysql';

  const queryToStatement = SQLQuery.toSQLStatement;

  async function executeQuery(sqlStatement: SQLStatement): Promise<unknown> {
    const connection = createConnection(client);

    try {
      connection.connect();
      const queryResultPromise = new Promise((resolve, reject) => {
        connection.query(sqlStatement, (error, result) => (error != null ? reject(error) : resolve(result)));
      });

      const queryResult = await queryResultPromise;

      return queryResult;
    } finally {
      connection.end();
    }
  }

  async function handleError(cause: unknown) {
    return Promise.resolve(DatabaseClientError({ cause }));
  }

  return {
    adapter,
    executeQuery,
    queryToStatement,
    handleError,
  };
}
