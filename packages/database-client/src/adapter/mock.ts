import { SQLStatement } from '../sql';
import { AbstractDatabaseAdapter, AbstractDatabaseClient } from '../client';
import { SQLQuery } from '../query';
import { DatabaseClientError } from '../error';

export interface MockClient extends AbstractDatabaseClient<'mock'> {
  mockExecuteQuery?: (sqlStatement: SQLStatement) => Promise<unknown>;
  mockQueryToStatement?: typeof SQLQuery.toSQLStatement;
}
export interface MockAdapter extends AbstractDatabaseAdapter<'mock'> {}
export function MockAdapter({
  mockExecuteQuery = () => Promise.reject(new Error('NotImplementedError')),
  mockQueryToStatement = SQLQuery.toSQLStatement,
}: MockClient): MockAdapter {
  const adapter = 'mock';

  async function handleError(cause: unknown) {
    return Promise.resolve(DatabaseClientError({ cause }));
  }

  return {
    adapter,
    executeQuery: mockExecuteQuery,
    queryToStatement: mockQueryToStatement,
    handleError,
  };
}
