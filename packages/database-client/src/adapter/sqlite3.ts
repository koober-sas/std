import { Database } from 'sqlite3';
import { SQLStatement } from '../sql';
import { AbstractDatabaseAdapter, AbstractDatabaseClient } from '../client';
import { SQLQuery } from '../query';
import { DatabaseClientError } from '../error';

export interface SQLite3Client extends AbstractDatabaseClient<'sqlite3'> {
  filename: string;
}

export interface SQLite3Adapter extends AbstractDatabaseAdapter<'sqlite3'> {}

export function SQLite3Adapter(client: SQLite3Client): SQLite3Adapter {
  const adapter = 'sqlite3';

  const queryToStatement = SQLQuery.toSQLStatement;

  function sqlite3SQLStatement({ sql, ...other }: SQLStatement): SQLStatement {
    // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
    return {
      ...other,
      sql: sql.replace(/UNIX_TIMESTAMP\(\)/, "strftime('%s','now')"),
    } as SQLStatement;
  }

  async function executeQuery(sqlStatement: SQLStatement): Promise<unknown> {
    const sqliteStatement = sqlite3SQLStatement(sqlStatement);
    const database = new Database(client.filename);
    const queryResultPromise = new Promise((resolve, reject) => {
      database.all(sqliteStatement.sql, sqliteStatement.values, (error, result) =>
        error != null ? reject(error) : resolve(result)
      );
    });

    try {
      const queryResult = await queryResultPromise;

      return queryResult;
    } finally {
      database.close();
    }
  }

  async function handleError(cause: unknown) {
    return Promise.resolve(DatabaseClientError({ cause }));
  }

  return {
    adapter,
    executeQuery,
    queryToStatement,
    handleError,
  };
}
