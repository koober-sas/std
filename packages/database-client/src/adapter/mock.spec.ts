/* eslint-disable promise/prefer-await-to-callbacks */
import { SQL } from '..';
import { MockAdapter } from './mock';

describe(MockAdapter, () => {
  const name = 'Toto';
  const anyStatement = SQL`SELECT author FROM books WHERE name=${name}`;
  const anyMock = MockAdapter({ databaseType: 'mock' });

  describe('.adapter', () => {
    test('should be "mock"', () => {
      expect(anyMock.adapter).toEqual('mock');
    });
  });

  describe('.executeQuery()', () => {
    test('should use mockExecuteQuery function', () => {
      const mockExecuteQuery = jest.fn(() => Promise.resolve(null));
      const adapter = MockAdapter({
        databaseType: 'mock',
        mockExecuteQuery,
      });

      expect(adapter.executeQuery).toBe(mockExecuteQuery);
    });
    test('should use mockQueryToStatement function', () => {
      const mockQueryToStatement = jest.fn(() => anyStatement);
      const adapter = MockAdapter({
        databaseType: 'mock',
        mockQueryToStatement,
      });

      expect(adapter.queryToStatement).toBe(mockQueryToStatement);
    });
  });
});
