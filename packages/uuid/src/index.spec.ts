import * as Module from '.';

describe('index', () => {
  test('exports', () => {
    expect(Object.keys(Module).sort()).toEqual(
      [
        // List of all public exports
        'UUID',
        'randomUUID',
      ].sort()
    );
  });
});
