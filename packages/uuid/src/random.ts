import { Task } from '@koober/std';
import { v4 as uuidV4 } from 'uuid';
import { UUID } from './data';

/**
 * A task that returns a new `UUID`
 *
 * @example
 * ```typescript
 * const createUser = (name: string) => Task.map(randomUUID, (uuid) => ({
 *   id: uuid,
 *   name,
 * }));
 * ```
 */
export const randomUUID: Task.Sync<UUID, never> = Task.Sync(({ ok }) => ok(uuidV4() as UUID));
