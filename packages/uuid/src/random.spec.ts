import { runTask, Result } from '@koober/std';
import * as UUIDImpl from 'uuid';
import { UUID } from './data';
import { randomUUID } from './random';

jest.mock('uuid', () => ({
  ...jest.requireActual('uuid'),
  // eslint-disable-next-line @typescript-eslint/no-unsafe-argument, @typescript-eslint/no-unsafe-member-access
  v4: jest.fn(jest.requireActual('uuid').v4),
}));

describe('randomUUID', () => {
  test('should return a valid UUID', () => {
    const uuidResult = Result.getOrThrow(runTask(randomUUID));
    expect(UUID.hasInstance(uuidResult)).toBe(true);
  });
  test('should use uuid v4 module', () => {
    const uuidMock = UUID.empty();
    const uuidV4 = jest.spyOn(UUIDImpl, 'v4');
    uuidV4.mockClear();
    uuidV4.mockReturnValue(uuidMock);
    const uuidResult = runTask(randomUUID);

    expect(uuidV4).toHaveBeenCalledTimes(1);
    expect(uuidResult).toEqual(Result.Ok(uuidMock));
  });
});
