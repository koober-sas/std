import { randomUUID } from '@koober/uuid';
import { runTask, Task } from '@koober/std';

function createUser(name: string) {
  return Task.map(randomUUID, (uuid) => ({
    id: uuid,
    name,
  }));
}

export function main(): void {
  const userTask = createUser('John Doe');
  console.log(runTask(userTask)); // > Result.Ok({ id: 'XXXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXX', name: 'John Doe' })
}
