# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# 1.0.0-rc.54 (2021-09-01)

**Note:** Version bump only for package @koober/uuid





# 1.0.0-rc.53 (2021-08-12)


### Bug Fixes

* setup correctly tsconfig.json ([7054880](https://gitlab.com/koober-sas/std/commit/70548804e7078d3f4ffd4d4fbcdc4b38fbf496b7))





# [1.0.0-rc.52](https://gitlab.com/koober-sas/std/compare/v1.0.0-rc.51...v1.0.0-rc.52) (2021-08-11)


### Features

* publish first version ([16317a8](https://gitlab.com/koober-sas/std/commit/16317a844106a7e313138af9e68095d4910afb2d))





# 1.0.0-rc.51 (2021-08-11)

**Note:** Version bump only for package @koober/uuid
